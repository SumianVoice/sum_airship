# sum_airship
Mod for minetest

[ContentDB](https://content.minetest.net/packages/Sumianvoice/sum_airship/)



This adds an airship to minetest_game or mcl2. Not tested on any other games. If it uses default:steel_ingot or mcl_core:iron_ingot for example, then it should work. It should not crash no matter what game you use though, this is only for the crafting recipes. The dependency on MTG or MCL2 is only for crafting, otherwise the ship it self can still be used just not crafted (because I don't know what your game calls each item / node etc)

Flies like a boat, WSAD, jump and aux1 (sprint) for up and down. Shift to dismount. Does not require fuel but will go faster when given any type of coal.
### Crafting (mcl)

Craftable with wool > canvas and boats > hull. All items: wool: 27, string: 4, wood: 15, iron_ingot: 4

#### Crafting Guide GIF:  
![airship%20crafting.gif](airship%20crafting.gif)
### Air Currents

This optionally uses [sum_air_currents](https://content.minetest.net/packages/Sumianvoice/sum_air_currents/) to apply force based on the weather. Both this mod and the air currents mod are in development so expect changes made regularly.
